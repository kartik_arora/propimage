# PropImage


## Introduction

ProgImage is a specialised image storage and processing engine which can be used by other applications, which provides high-performance programmatic access via its RESTful API. Apart from bulk image storage and retrieval, ProgImage provides a number of image processing and transformation capabilities such as compression, rotation, a variety of filters, thumbnail creation, and masking.

## Tech Specs

* Python 3.7
* Boto3 
* Pillow 6.0.0 (Image processing) 
* Python Unittest
* Pynamodb 3.3.3 - DynamoDB ORM Layer 
* AWS Lambda - Serverless
* AWS API Gateway
* AWS S3 - Storing image
* AWS Cloudwatch - Logging
* Serverless Framework - Build and deploy AWS Stack
* Bitbucket Pipelines - Deployment

## API Information

 `https://{{api_gateway_path}}/{{environment}}/{{resource}}`

 

> api_gateway_path

https://81qil6lr50.execute-api.ap-south-1.amazonaws.com/

> environment
 
 development or production


## Architecture

![enter image description here](https://i.imgur.com/wu5qPwi.png)

## Getting started

There are currently 2 endpoints to upload the image and fetch the image based on the ID.


 Resource      | Method     | Description
:--------------|:----------:|----------------------|
 `/images`     | POST       | post single or multiple images with a max limit of 10 images
 `/image/{id}` | GET        | get image URL which is uploaded


## Image Upload

Upload a new image in base64 format.

> Max image size is **60MB.**
> Accepted MIME type are **jpg, png, gif**
> Image is uploaded in base64 format

``` http
POST /images
```
##### Request Headers

```json
Content-Type : application/json
```
##### Request body

```json
{
    "images": [
        {
            "image_name": "test1.png",
            "image": "base64"
        },
        {
            "image_name": "test2.jpg",
            "image": "base64"
        }
    ]
}
```

##### Sample Response

```json
{
    "message": [
        {
            "id": "fc7b0eac-6081-11e9-b5dc-ca451191e3d9",
            "image_path": "https://s3.ap-south-1.amazonaws.com/propimage-upload/test_image23212_1555444701201.png",
            "other_formats": [
                {
                    "image_path": "https://s3.ap-south-1.amazonaws.com/propimage-upload/test_image23212_1555444701201.jpeg"
                },
                {
                    "image_path": "https://s3.ap-south-1.amazonaws.com/propimage-upload/test_image23212_1555444701201.gif"
                }
            ]
        },
        {
            "id": "fce1ca66-6081-11e9-b5dc-ca451191e3d9",
            "image_path": "https://s3.ap-south-1.amazonaws.com/propimage-upload/test2_1555444701201.png",
            "other_formats": [
                {
                    "image_path": "https://s3.ap-south-1.amazonaws.com/propimage-upload/test2_1555444701201.jpeg"
                },
                {
                    "image_path": "https://s3.ap-south-1.amazonaws.com/propimage-upload/test2_1555444701201.gif"
                }
            ]
        }
    ]
}
```


## Fetch Image

Fetch uploaded image by an ID

``` http
POST /images/{id}
```
##### Sample Response

```json
{
    "message": {
        "url": "https://propimage-upload.s3.amazonaws.com/test_image23212_1555365755887.gif?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAZXXB4UGZXLU4ZVTV%2F20190415%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Date=20190415T221021Z&X-Amz-Expires=60&X-Amz-SignedHeaders=host&X-Amz-Security-Token=FQoGZXIvYXdzEEAaDBUoASZQa%2FgsTKOCHSL1Aau%2B1dtyngk4L0WWBFirmhEt4YavzdDQ7DEJWwy%2FKxuNJogMegRVCRgcCtI6zfWMcyqTvsTTW3ZQMNnyqOfc1JMjE98IRNygXLnu1w03FapkMI17ExLxjFLEkweNo5rZ%2B6d8kE6Y0ubAtv6S%2BeGchFfbtYot5KilAGm0ZIshYLdfAZg0eVm0Rq4FyXQRhg4veNg6ZgamrqItp585rC6O%2B5b6EtqCOqNpMUuFyPMPCbnOFKqfbL2Ke0EVjXYrEnkrs1XdSeZz3zhB2tbRwryhGWM%2F77xvDJTlsSRVox60sOkhBbw2FQuFVkxywqbaSmDgbWipY46qKM2C1OUF&X-Amz-Signature=fef52c623c22e7b9663f7d3c8c1cfae13e0b0098b251ab9f434a86ad0f195691"
    }
}
```

## Postman Collection
[Click to see the collection](https://documenter.getpostman.com/view/3058721/S1ERuwFt)
