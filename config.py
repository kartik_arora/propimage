import os
import time


ALLOWED_EXTENSIONS = ['png', 'jpeg', 'gif']
ALLOWED_SIZE = 32
REGION = os.environ['AWS_S3_REGION']
BUCKET = os.environ['AWS_S3_BUCKET']
OTHER_FORMATS = True
DYNAMODB_TABLE=os.environ['DYNAMODB_TABLE']
DYNAMODB_HOST= os.environ['DYNAMODB_HOST']
TIMESTAMP = int(time.time() * 1000)
