import os
import http.client as httplib
from pynamodb.exceptions import DoesNotExist
from asset.asset_model import AssetModel
from log_cfg import logger
import json
from asset import common


def get(event, context):
    
    logger.debug('event: {}'.format(event))
    try:
        ttl = os.environ['URL_DEFAULT_TTL']
        try:
            ttl = int(event['query']['timeout'])
        except KeyError or ValueError:
            pass
        logger.debug('eventId: {}'.format(event['pathParameters']['id']))
        id = event['pathParameters']['id']
        asset = AssetModel.get(hash_key=id)
        download_url = asset.get_download_url(ttl)

    except DoesNotExist:
        return common.response({'message': 'Image {} not found'.format(id)},httplib.FORBIDDEN)

    except AssertionError as e:
        return common.response({'message': 'Unable to download: {}'.format(e)},httplib.FORBIDDEN)
    
    return common.response({'url': download_url},httplib.OK)
