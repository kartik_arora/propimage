import json
import os
import uuid
import base64
from asset import common
from asset.asset_model import AssetModel
import http.client as httplib
import boto3
from PIL import Image
import PIL.Image
from log_cfg import logger
from io import BytesIO
import config


# s3 = boto3.resource('s3')
# dynamodb = boto3.resource('dynamodb')

def upload(event, context):
    data = json.loads(event['body'])
    responseImageURL = []
    if 'images' not in data:
        logger.error('Validation Failed')
        raise Exception("images array not available")
        return

    # validation if total images count is greater than allowed
    if len(data['images']) > 10:
        res = {'message': 'More than 10 images are not allowed'}
        return common.response(res,httplib.BAD_REQUEST)

    for image in data['images']:
        fileName = image['image_name']
        image_base64 = image['image']
        imageBase64Trim = image_base64.partition(",")[2]
        # base64 validation
        if not common.isBase64(imageBase64Trim):
            res = {'message': 'Image is not valid'}
            return common.response(res,httplib.BAD_REQUEST)
        else:
            contentEncoding = 'base64'
        # image extension validation
        fileType = image_base64.split("data:")[1].split(";base64,")[0]
        if not common.allowedFile(fileType):
            res = {'message': 'Image extension not allowed'}
            return common.response(res,httplib.PRECONDITION_FAILED)
        # file size validation
        if not common.imageSize(image_base64):
            res = {'message': 'Image size is greater than '+common.ALLOWED_SIZE+'MB is not allowed.'}
            return common.response(res,httplib.PRECONDITION_FAILED)

        base64decodeImage = base64.b64decode(imageBase64Trim)
        #update file name with time - unique name for the file to avoid overwrite
        fileNameWithoutExtension = fileName.rsplit('.', 1)[0].lower()
        fileNameExtension = fileType.rsplit('/', 1)[1].lower()
        uniqueFileName = "{}_{}.{}".format(fileNameWithoutExtension, config.TIMESTAMP, fileNameExtension)
        s3Response = common.uploadImageS3(uniqueFileName, base64decodeImage, fileType, fileNameExtension, contentEncoding)
        if s3Response:
            #table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])
            uniqueIdentifier = uuid.uuid1().__str__()
            asset = AssetModel()
            asset.id = uniqueIdentifier
            asset.image_name = uniqueFileName
            asset.image_path = s3Response
            asset.image_type = fileType
            asset.save()

        #check for other formats flag is on then upload images in other formats
        if config.OTHER_FORMATS:
            otherFormats = common.uploadOtherFormats(base64decodeImage, fileNameExtension, fileNameWithoutExtension)
            logger.info(otherFormats)
            responseImageURL.append({'id' : asset.id, 'image_path' : s3Response, 'other_formats' : otherFormats})
        else:
            responseImageURL.append({'id' : asset.id, 'image_path' : s3Response})
        
    return common.response(responseImageURL,httplib.CREATED)
            