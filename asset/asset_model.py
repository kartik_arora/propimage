import os
from datetime import datetime
import boto3
from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute
from log_cfg import logger
import config

class AssetModel(Model):
    class Meta:
        table_name = config.DYNAMODB_TABLE
        region = config.REGION
        host = config.DYNAMODB_HOST
        # 'https://dynamodb.us-east-1.amazonaws.com'

    id = UnicodeAttribute(hash_key=True)
    createdAt = UTCDateTimeAttribute(null=False, default=datetime.now().astimezone())
    updatedAt = UTCDateTimeAttribute(null=False, default=datetime.now().astimezone())
    image_name = UnicodeAttribute(null=False)
    image_path = UnicodeAttribute(null=False)
    image_type = UnicodeAttribute(null=False)

    def __str__(self):
        return 'id:{}'.format(self.id)

    def save(self, conditional_operator=None, **expected_values):
        try:
            self.updatedAt = datetime.now().astimezone()
            logger.info('saving: {}'.format(self))
            super(AssetModel, self).save()
        except Exception as e:
            logger.error('save {} failed: {}'.format(self.id, e), exc_info=True)
            raise e

    def get_download_url(self, ttl=60):
        """
        :param ttl: url duration in seconds
        :return: a temporary presigned download url
        """
        s3 = boto3.client('s3')
        get_url = s3.generate_presigned_url(
            'get_object',
            Params={
                'Bucket': config.BUCKET,
                'Key': self.image_name,
            },
            ExpiresIn=ttl,
            HttpMethod='GET'
        )
        logger.debug('download URL: {}'.format(get_url))
        return get_url