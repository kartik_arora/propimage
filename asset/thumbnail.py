import http.client as httplib
import os
from pynamodb.exceptions import DoesNotExist, DeleteError, UpdateError
from asset.asset_model import AssetModel
from PIL import Image
import PIL.Image
from log_cfg import logger
from io import BytesIO

def thumbnail(event, context):
    """
    Triggered by s3 events, object create

    """

    logger.debug('event: {}'.format(event))
    event_name = event['Records'][0]['eventName']
    key = event['Records'][0]['s3']['object']['key']
    #asset_id = key.replace('{}/'.format(os.environ['S3_KEY_BASE']), '')
    logger.debug('key: {}'.format(key))
    logger.debug('event_name: {}'.format(event_name))
    s3_client = boto3.client('s3')

    try:
        if 'ObjectCreated:Put' == event_name:

            try:
                resizedImage = BytesIO()
                with Image.open(image_path) as image:
                    image.thumbnail(tuple(x / 2 for x in image.size))
                    image.save(resizedImage)
                    s3_client.upload_file(resizedImage, '{}resized'.format(bucket), key)

            except UpdateError:
                return {
                    'statusCode': httplib.BAD_REQUEST,
                    'body': {
                        'error_message': 'Unable to update ASSET'}
                }

    except DoesNotExist:
        return {
            'statusCode': httplib.NOT_FOUND,
            'body': {
                'error_message': 'ASSET {} not found'.format(asset_id)
            }
        }

    return {'statusCode': httplib.ACCEPTED}
