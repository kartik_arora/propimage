import json
import os
import boto3
import base64
import http.client as httplib
from PIL import Image
import PIL.Image
from log_cfg import logger
from io import BytesIO
import config

def response(res, code):
    if code == httplib.PRECONDITION_FAILED or code == httplib.BAD_REQUEST or code == httplib.FORBIDDEN:
        response = {'statusCode': code, 'body': json.dumps({'error': res})}
    elif code == httplib.OK or httplib.NO_CONTENT:
        response = {'statusCode': code, 'body': json.dumps({'message': res})}
    return response

def allowedFile(fileType):
    fileExtension = fileType.rsplit('/', 1)[1].lower()
    if fileExtension in config.ALLOWED_EXTENSIONS:
        return True
    else:
        return False

def imageSize(b64string):
	size = ((len(b64string) * 3) / 4 - b64string.count('=', -2))/1000000
	if size > config.ALLOWED_SIZE:
		return False
	else:
		return True

def isBase64(sb):
        try:
                if type(sb) == str:
                        # If there's any unicode here, an exception will be thrown and the function will return false
                        sb_bytes = bytes(sb, 'ascii')
                elif type(sb) == bytes:
                        sb_bytes = sb
                else:
                        raise ValueError("Argument must be string or bytes")
                return base64.b64encode(base64.b64decode(sb_bytes)) == sb_bytes
        except Exception:
                return False

def uploadOtherFormats(base64decodeImage, fileNameExtension, fileNameWithoutExtension):
    buff = BytesIO(base64decodeImage)
    openImage = Image.open(buff)
    extraImages = []
    emptyBuffer = BytesIO()
    for ext in config.ALLOWED_EXTENSIONS:
        if ext != fileNameExtension:
            openImage.convert('RGB').save(emptyBuffer,ext.upper())
            logger.info(emptyBuffer)
            emptyBuffer.seek(0)
            contentType = 'image/{}'.format(ext)
            logger.debug('contentType: {}'.format(contentType))
            uniqueFileName = "{}_{}.{}".format(fileNameWithoutExtension, config.TIMESTAMP, ext)
            s3Response = uploadImageS3(uniqueFileName, emptyBuffer, contentType, ext)
            extraImages.append({'image_path' : s3Response})
            logger.debug(extraImages)
    return extraImages


def uploadImageS3(fileName, fileBody, fileType, fileExtension, contentEncoding='base64'):
    s3 = boto3.resource('s3')
    s3Object = s3.Object(config.BUCKET, fileName)
    s3Response = s3Object.put(Body=fileBody, ContentType=fileType
                              , ContentEncoding=contentEncoding)
    logger.debug('s3Response: {}'.format(s3Response))
    if s3Response['ResponseMetadata']['HTTPStatusCode'] == 200:
        return 'https://s3.{}.amazonaws.com/{}/{}'.format(config.REGION,
                config.BUCKET, fileName)
    else:
        return False
